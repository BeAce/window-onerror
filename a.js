window.addEventListener("error", event => {
  console.log(event);
});

const b = new B("hello B");
b.sayName();

const c = new C("hello C");
c.sayName();

document.getElementById("btn-1").addEventListener("click", event => {
  b.hello();
});

document.getElementById("btn-2").addEventListener("click", event => {
  c.hello();
});
